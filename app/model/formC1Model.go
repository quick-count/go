package model

import "gitlab.com/quick-count/go/helper"

type FormC1Payload struct {
	ID                    string `json:"form_id" `
	UserID                string `json:"user_id" validate:"required"`
	TpsID                 string `json:"tps_id" validate:"required"`
	HasilSuaraSatu        uint   `json:"hasil_suara_satu" `
	HasilSuaraKotakKosong uint   `json:"hasil_suara_kosong" `
	TotalSuaraMasuk       uint   `json:"total_suara" `
	SuaraTidakSah         uint   `json:"suara_tidak_sah"`
	helper.TimeModel
}

type FormC1Model struct {
	ID                    uint `json:"-" `
	UserID                uint `json:"-" `
	TpsID                 uint `json:"-" `
	HashID                string `json:"id" `
	HasilSuaraSatu        uint `json:"hasil_suara_satu" `
	HasilSuaraKotakKosong uint `json:"hasil_suara_kosong"`
	TotalSuaraMasuk       uint `json:"total_suara"`
	SuaraTidakSah         uint `json:"suara_tidak_sah"`
	Link 				  string
	helper.TimeModel
}

type FormC1Model2 struct {
	ID                    uint `json:"-" `
	Name  				  string `json:"name"`
	HashID                string `json:"id" `
	HasilSuaraSatu        uint `json:"hasil_suara_satu" `
	HasilSuaraKotakKosong uint `json:"hasil_suara_kosong"`
	TotalSuaraMasuk       uint `json:"total_suara"`
	SuaraTidakSah         uint `json:"suara_tidak_sah"`
	Link 				  string `json:"link"`
	helper.TimeModel
}
func (FormC1Model) TableName() string {
	return "form_c1"
}