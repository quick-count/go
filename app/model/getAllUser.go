package model

import "gorm.io/gorm"

type RequestGetAllUser struct{
	Jenis string
	Text string
}

type ResponseGetAllUser struct {
	Id string `json:"id" `
	Tps_id string `json:"tps_id" `
	Username string `json:"username" `
	Nama_tps string `json:"nama_tps" `
	Kecid string `json:"kecid" `
	Kelid string `json:"kelid" `
	Kec string `json:"kec" `
	Kel string `json:"kel" `
}


func GetAllUser(db *gorm.DB,request RequestGetAllUser) (interface{}, error) {
	whereQuery := " WHERE "
	switch request.Jenis {
	case "Username":
		whereQuery += " users.username  LIKE ?"
	case "Kelurahan":
		whereQuery += " tps.kel   LIKE ?"
	case "Kecamatan":
		whereQuery += " tps.kec  LIKE ?"

	}
	tmp := []ResponseGetAllUser{}
	query := "SELECT " +
			"users.hash_id 'id'," +
			"users.username 'username'," +
			"tps.hash_id 'tps_id' , " +
			"tps.name 'nama_tps', " +
			"tps.hashkecid 'kecid', " +
			"tps.hashkelid 'kelid'," +
			"tps.kec 'kec', " +
			"tps.kel 'kel'" +
		"FROM " +
			"users " +
		"INNER JOIN saksi ON users.saksi_id=saksi.id " +
		"INNER JOIN (SELECT * FROM tps " +
		"				INNER JOIN (SELECT " +
		"					kecamatans.id 'kecid'," +
		"					kecamatans.hash_id 'hashkecid'," +
		"					kelurahans.id 'kelid'," +
		"					kelurahans.hash_id 'hashkelid'," +
		"					kecamatans.name 'kec'," +
		"					kelurahans.name 'kel'" +
		"				FROM kecamatans " +
		"				INNER JOIN kelurahans ON kecamatans.id=kelurahans.kecamatan_id) kel " +
		"ON tps.kelurahan_id=kel.kelid) tps ON users.tps_id=tps.id  "+whereQuery
	if err := db.Raw(query,"%"+request.Text+"%").Scan(&tmp).Error;err != nil {
		return nil, err
	}
	return tmp,nil
}