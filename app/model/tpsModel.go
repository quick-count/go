package model

import (
	"errors"
	"gitlab.com/quick-count/go/helper"
	"gorm.io/gorm"
)

type TPS struct {
	ID        		uint `json:"hashid" gorm:"primarykey"`
	HashID 			string `json:"id"  gorm:"type:varchar(255);"`
	Name			string `json:"nama"  validate:"required,email" gorm:"type:varchar(255);uniqueIndex"`
	Alamat			string `json:"alamat"  validate:"required" gorm:"type:varchar(255);uniqueIndex"`
	KelurahanID 	string `json:"kelurahan_id"   gorm:"type:varchar(255);"`
	//KecKel			interface{}
}

type GetKec struct {
	ID string `json:"-"`
	HashKelID string `json:"kel_id"`
	HashKecID string `json:"kec_id"`
	Kel string `json:"kelurahan"`
	Kec string `json:"kecamatan"`
}

func (data *GetKec) getTPS(db *gorm.DB,tpsid uint) error  {
	check := db.Raw("SELECT kel.* " +
		"FROM tps INNER JOIN (" +
		"SELECT kelurahans.id 'kel_id', " +
		"kelurahans.hash_id 'hash_kel_id', " +
		"kecamatans.hash_id 'hash_kec_id', " +
		"kelurahans.name 'kel', kecamatans.name 'kec'  " +
		"FROM kelurahans  " +
		"INNER JOIN kecamatans  ON kelurahans.kecamatan_id=kecamatans.id) kel " +
		"ON kel.kel_id=tps.kelurahan_id where tps.id=?", tpsid).Scan(&data)
	if check.Error != nil {
		return check.Error
	}

	return nil
}

func (data *TPS) setKec(db *gorm.DB) (interface{},error) {
	getKec := GetKec{}
	if err := getKec.getTPS(db,data.ID); err != nil {
		return nil,err
	}
	return getKec,nil
}

func (data TPS) Create(db *gorm.DB) (interface{},error) {
	tx := db.Begin()
	result := tx.Create(&data)
	if result.Error != nil {
		tx.Rollback()
		return nil,result.Error
	}
	if result.RowsAffected < 1 {
		tx.Rollback()
		return nil,errors.New("Failed to create data")
	}
	if err := data.updateHashId(tx);err != nil{
		tx.Rollback()
		return nil, err
	}
	tx.Commit()
	return data,nil
}

func (data *TPS) updateHashId(db *gorm.DB)  error {
	enc,err := helper.EncodeHash(int(data.ID))
	if err != nil {
		return err
	}
	response := db.Model(&data).Update("hash_id", enc)
	if response.Error != nil{
		return response.Error
	}
	return nil
}

