package model

import (
	"errors"
	"gitlab.com/quick-count/go/helper"
	"gorm.io/gorm"
	"net/http"
	"strconv"
	"time"
)

type ImgFormC1 struct {
	ID uint
	Link string
	FormC1ID uint
	UserID uint
}

func (ImgFormC1) TableName() string {
	return "image_form_c1"
}

func (data *FormC1Model) UploadImg(db *gorm.DB,r *http.Request,userId string) (interface{},error){
	userID,err := helper.DecodeHash(userId)
	if err != nil {
		return nil,err
	}
	tx := db.Begin()
	check := tx.Where("user_id = ?", userID).First(&data)
	if err = check.Error; err != nil {
		return nil,err
	}
	if check.RowsAffected < 1  {
		return nil,errors.New("Form c1 belum di inputkan")
	}
	img := ImgFormC1{}
	var count int64
	db.Model(&ImgFormC1{}).Where("user_id = ?", userID).Count(&count)
	//db.Where().Table("image_form_c1").Count(&count)
	if count > 3  {
		tx.Rollback()
		return nil,errors.New("Anda sudah tidak dapat merubah data lagi")
	}
	filename := helper.CreateHashMd5(helper.GenerateTime().Format(time.RFC3339)+strconv.Itoa(int(data.ID)))
	filename,err = helper.ImgProcess(r,"c1",filename)
	if err != nil{
		tx.Rollback()
		return nil,err
	}
	img.UserID = userID
	img.Link = filename
	img.FormC1ID = data.ID
	check = tx.Create(&img)
	if check.Error != nil {
		tx.Rollback()
		return nil,check.Error
	}
	if check.RowsAffected < 1 {
		tx.Rollback()
		return nil,errors.New("Gagal upload data")
	}
	data.Link = img.Link
	check = tx.Save(&data)
	if check.Error != nil {
		tx.Rollback()
		return nil,check.Error
	}
	if check.RowsAffected < 1 {
		tx.Rollback()
		return nil,errors.New("Gagal upload data")
	}
	tx.Commit()
	return data,nil
}

func (data *SaksiCustom) UploadImg(db *gorm.DB,r *http.Request,userId string) (interface{},error){
	userID,err := helper.DecodeHash(userId)
	if err != nil {
		return nil,err
	}

	tx := db.Begin()
	user := Users{}
	check := tx.Where("id = ?", userID).First(&user)
	if err = check.Error; err != nil {
		return nil,err
	}
	check = tx.Where("id = ?", user.SaksiID).First(&data)
	if err = check.Error; err != nil {
		return nil,err
	}
	if check.RowsAffected < 1  {
		return nil,errors.New("Saksi belum di inputkan")
	}
	filename := helper.CreateHashMd5(helper.GenerateTime().Format(time.RFC3339)+strconv.Itoa(int(data.ID)))
	filename,err = helper.ImgProcess(r,"ktp",filename)
	if err != nil{
		tx.Rollback()
		return nil,err
	}
	data.Identity = filename
	check = tx.Save(&data)
	if check.Error != nil {
		tx.Rollback()
		return nil,check.Error
	}
	if check.RowsAffected < 1 {
		tx.Rollback()
		return nil,errors.New("Gagal upload data")
	}
	tx.Commit()
	return data,nil
}
