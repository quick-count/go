package model

import "gitlab.com/quick-count/go/helper"

type Users struct {
	ID        		uint `json:"-" gorm:"primarykey"`
	HashID 			string `json:"id"  gorm:"type:varchar(255);"`
	Email			string `json:"email"  validate:"required,email" gorm:"type:varchar(255);uniqueIndex"`
	Username		string `json:"username"  validate:"required" gorm:"type:varchar(255);uniqueIndex"`
	Password 		string `json:"password"   gorm:"type:varchar(255);"`
	Role			string `json:"role"  validate:"required"  gorm:"type:varchar(255);"`
	SaksiID			uint `json:"saksi_id"  `
	TpsID			uint `json:"tps_id"  `
	Pass 			string
	helper.TimeModel
}

type UsersPayload struct {
	ID        		uint `json:"-" gorm:"primarykey"`
	HashID 			string `json:"id"  gorm:"type:varchar(255);"`
	Email			string `json:"email"  validate:"required,email" gorm:"type:varchar(255);uniqueIndex"`
	Username		string `json:"username"  validate:"required" gorm:"type:varchar(255);uniqueIndex"`
	Role			string `json:"role"  validate:"required"  gorm:"type:varchar(255);"`
	SaksiID			uint `json:"-"  `
	TpsID			uint `json:"-"  `
	Tps 			TPS `json:"tps,omitempty"  gorm:"foreignkey:TpsID;references:ID"`
	Saksi			SaksiCustom `json:"saksi,omitempty"  gorm:"foreignkey:SaksiID;references:ID"`
	//FormC1			FormC1Payload	 `json:"form_c1,omitype"  gorm:"foreignkey:UserID;references:ID"`
	helper.TimeModel
}

type CustomUserPayload struct {
	UsersPayload
	Daerah interface{}
}
func (data *UsersPayload) init()  {
	data.Saksi = SaksiCustom{}
	data.Tps = TPS{}
	//data.Kelurahan = Kelurahan{}
	//data.Kecamatan = Kecamatan{}
	//data.Saksi = Saksi{}
	//data.FormC1 = FormC1Payload{}
}

func (SaksiCustom) TableName() string {
	return "saksi"
}

func (UsersPayload) TableName() string {
	return "users"
}



func (Users) TableName() string {
	return "users"
}