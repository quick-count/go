package model

import (
	"gitlab.com/quick-count/go/helper"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)

func (data Saksi) All(db *gorm.DB,string ...string) (interface{}, error) {
	result := []Saksi{}
	limit,err := strconv.Atoi(string[1])
	if err != nil {
		return nil, err
	}
	trans := db.Limit(limit).Joins("Users").Find(&result)
	hashID := string[0]
	if hashID != "" {
		id,err := helper.DecodeHash(hashID)
		if err != nil {
			return nil,err
		}
		trans = trans.Where("id > ?",id).Find(&result)
	}
	exec := trans.Find(&result)
	if exec.Error != nil {
		return result,exec.Error
	}
	return result,nil
}

func (data *Saksi) Find(db *gorm.DB,string ...string)   (interface{},error){
	hashid := string[0]
	id,err := helper.DecodeHash(hashid)
	if err = db.Where("id = ?", id).First(&data).Error; err != nil {
		return nil,err
	}
	return data,nil
}


func (data Saksi) Update(db *gorm.DB,r *http.Request, string ...string)  (interface{},error) {
	hashid := string[0]
	id,err := helper.DecodeHash(hashid)
	if err != nil {
		return nil,err
	}
	if err := db.Where("id = ?", id).First(&data).Error; err != nil {
		return nil,err
	}
	if err := data.setValue(false,r);err != nil {
		return nil, err
	}
	if err := db.Save(&data).Error; err != nil {
		return nil,err
	}
	return data,nil
}

func (data Saksi) Delete(db *gorm.DB,string ...string) (interface{},error){
	hashid := string[0]
	id,err := helper.DecodeHash(hashid)
	if err != nil {
		return nil,err
	}
	tx := db.Begin()
	if err := tx.Where("id = ?", id).First(&data).Error; err != nil {
		tx.Rollback()
		return nil,err
	}
	response := tx.Where("saksi_id = ?",id).Delete(Users{})
	if response.Error != nil {
		tx.Rollback()
		return nil,response.Error
	}
	response = tx.Where("id = ?",id).Delete(Saksi{})
	if response.Error != nil {
		tx.Rollback()
		return nil,response.Error
	}
	tx.Commit()
	return data,nil
}

func (data *Saksi) setValue(update bool, r *http.Request)  error  {
	if err := helper.ValidateData(data);err != nil {
		return err
	}
	data.GenerateTime(update)
	//data.Users.GenerateTime(update)
	return nil
}

func (data *Saksi) updateHashId(db *gorm.DB)  error {
	enc,err := helper.EncodeHash(int(data.ID))
	if err != nil {
		return err
	}
	response := db.Model(&data).Update("hash_id", enc)
	if response.Error != nil{
		return response.Error
	}
	return nil
}
