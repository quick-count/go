package model

import (
	"gitlab.com/quick-count/go/helper"
	"gorm.io/gorm"
	"strconv"
	"strings"
)

type RekapSuara struct {
	HasilSuaraSatu        uint `json:"hasil_suara_satu" `
	HasilSuaraKotakKosong uint `json:"hasil_suara_kosong"`
	SuaraTidakSah         uint `json:"suara_tidak_sah"`
	TotalSuaraMasuk       uint `json:"total_suara"`
}

type RekapSuaraDaerah struct {
	Name 				  string
	HashID				  string
	HasilSuaraSatu        uint `json:"hasil_suara_satu" `
	HasilSuaraKotakKosong uint `json:"hasil_suara_kosong"`
	SuaraTidakSah         uint `json:"suara_tidak_sah"`
	TotalSuaraMasuk       uint `json:"total_suara"`
}

type GetDaerahName struct{
	Kecid string
	Kelid string
	Kelurahan string
	Kecamatan string
}


func (r *RekapSuara) GetRekap(db *gorm.DB) (interface{}, error) {
	var count int64
	db.Table("form_c1").Count(&count)
	if err := db.Raw("SELECT SUM(hasil_suara_satu) 'hasil_suara_satu', SUM(hasil_suara_kotak_kosong) 'hasil_suara_kotak_kosong',SUM(suara_tidak_sah) 'suara_tidak_sah',SUM(total_suara_masuk) 'total_suara_masuk' FROM form_c1").Scan(&r).Error;err != nil {
		return nil, err
	}
	tmp := struct {
		Rekap *RekapSuara
		TotalForm int64
	}{Rekap: r,TotalForm: count}
	return tmp, nil
}
func (r *RekapSuara) GetRekapByKelurahans(db *gorm.DB, string2 ...string) (interface{}, error) {
	tmp := []RekapSuaraDaerah{}
	id,err := helper.DecodeHash(string2[0])
	if err != nil{
		return nil,err
	}
	tmpDaerah := GetDaerahName{}
	if err := db.Raw("SELECT " +
		"kecamatans.id 'Kecid'," +
		"kelurahans.id 'Kelid'," +
		"kecamatans.name 'Kecamatan',  " +
		"kelurahans.name 'Kelurahan' " +
		"FROM kecamatans " +
		"INNER JOIN kelurahans " +
		"ON kecamatans.id=kelurahans.kecamatan_id " +
		"WHERE kelurahans.id=?",id).Scan(&tmpDaerah).Error;err != nil {
		return nil, err
	}
	if err := db.Raw("SELECT " +
			"tps.name 'Name',  " +
			"tps.hash_id 'HashID',  " +
			"hasil_suara_satu, " +
			"hasil_suara_kotak_kosong, " +
			"suara_tidak_sah, " +
			"total_suara_masuk " +
		"FROM form_c1 " +
			"INNER JOIN tps " +
			"ON form_c1.tps_id=tps.id " +
		"WHERE tps.kelurahan_id=?",id).Scan(&tmp).Error;err != nil {
		return nil, err
	}
	ta := struct {
		Title string
		Rekap []RekapSuaraDaerah
	}{Title: "KEC. "+strings.ToUpper(tmpDaerah.Kecamatan)+" KEL. "+strings.ToUpper(tmpDaerah.Kelurahan),Rekap: tmp}
	return ta, nil
}



func (r *RekapSuara) GetRekapByKecamatans(db *gorm.DB, string2 ...string) (interface{}, error) {
	tmp := []RekapSuaraDaerah{}
	id,err := helper.DecodeHash(string2[0])
	if err != nil{
		return nil,err
	}
	idString := strconv.Itoa(int(id))
	tmpDaerah := GetDaerahName{}
	if err := db.Raw("SELECT " +
		"kecamatans.id 'Kecid'," +
		"kelurahans.id 'Kelid'," +
		"kecamatans.name 'Kecamatan',  " +
		"kelurahans.name 'Kelurahan' " +
		"FROM kecamatans " +
		"INNER JOIN kelurahans " +
		"ON kecamatans.id=kelurahans.kecamatan_id " +
		"WHERE kelurahans.kecamatan_id=?",id).Scan(&tmpDaerah).Error;err != nil {
		return nil, err
	}
	if err := db.Raw("" +
		"SELECT" +
			" tps.kelname 'Name'," +
			" tps.kecid 'HashID'," +
			" SUM(hasil_suara_satu) 'hasil_suara_satu'," +
			" SUM(hasil_suara_kotak_kosong) 'hasil_suara_kotak_kosong'," +
			" SUM(suara_tidak_sah) 'suara_tidak_sah'," +
			" SUM(total_suara_masuk) 'total_suara_masuk' " +
		"FROM form_c1  " +
		"INNER JOIN " +
		"(SELECT " +
			" kelurahans.id 'kelid', " +
			" kelurahans.name 'kelname'," +
			" kelurahans.kecamatan_id 'kecid'," +
			" tps.id 'tpsid'," +
			" tps.hash_id 'tpshashid', " +
			" tps.kelurahan_id 'kelid2'," +
			" tps.name 'name' " +
		"FROM kelurahans " +
		" INNER JOIN" +
			" kecamatans          " +
			" ON kelurahans.kecamatan_id=kecamatans.id " +
			" INNER JOIN tps ON tps.kelurahan_id=kelurahans.id) tps      " +
			" ON form_c1.tps_id=tps.tpsid " +
		" WHERE tps.kecid="+idString+" Group by tps.kelname").Scan(&tmp).Error;err != nil {
		return nil, err
	}

	ta := struct {
		Title string
		Rekap []RekapSuaraDaerah
	}{Title: "KEC. "+strings.ToUpper(tmpDaerah.Kecamatan),Rekap: tmp}
	return ta, nil
}


func (data FormC1Model) AllByKelurahan(db *gorm.DB,string ...string)   (interface{},error){
	hashid := string[0]
	tmp := []FormC1Model2{}
	id,err := helper.DecodeHash(hashid)
	if err != nil {
		return nil, err
	}
	if err := db.Raw("SELECT " +
		"form_c1.*, " +
		"tps.name 'Name' " +
		"FROM form_c1 " +
		"INNER JOIN tps " +
		"ON form_c1.tps_id=tps.id " +
		"WHERE tps.kelurahan_id=?",id).Scan(&tmp).Error;err != nil {
		return nil, err
	}
	return tmp,nil
}



//func (r *RekapSuara) GetRekapByKelurahans(db *gorm.DB, string2 ...string) (interface{}, error) {
//	if err := db.Raw("SELECT " +
//		"SUM(hasil_suara_satu) 'hasil_suara_satu',  " +
//		"SUM(hasil_suara_kotak_kosong) 'hasil_suara_kotak_kosong', " +
//		"SUM(suara_tidak_sah) 'suara_tidak_sah', SUM(total_suara_masuk) 'total_suara_masuk' " +
//		"FROM form_c1 " +
//		"INNER JOIN tps " +
//		"ON form_c1.tps_id=tps.id " +
//		"WHERE tps.kelurahan_id=?",string2[0]).Scan(&r).Error;err != nil {
//		return nil, err
//	}
//	return r, nil
//}

