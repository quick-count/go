package model

import (
	"errors"
	"gitlab.com/quick-count/go/helper"
	"gorm.io/gorm"
	"log"
	"net/http"
	"strconv"
	"strings"
)

func (data *Users) Auth(db *gorm.DB, username,password string) error{
	pass := helper.Password{}
	if err := db.Where("username = ?",username ).First(&data).Error; err != nil {
		return err
	}
	if pass.Check(password,data.Password) != true{
		return errors.New("Username atau password salah")
	}
	return nil
}

func (data Users) FindOri(db *gorm.DB,string ...string)   (interface{},error){
	user := UsersPayload{}
	id,err := helper.DecodeHash(string[0])
	if err != nil {
		return nil, err
	}
	user.init()
	result := db.Joins("Saksi").Joins("Tps").Where("users.id = ?", id).Find(&user)
	if err := result.Error; err != nil {
		return nil,err
	}
	if result.RowsAffected < 1 {
		return nil,errors.New("Data tidak ditemukan")
	}
	//test := CustomUserPayload{}
	kec,err := user.Tps.setKec(db)
	if err != nil {
		return nil, err
	}
	test := CustomUserPayload{user,kec}
	return test,nil
}
func (data Users) Find(db *gorm.DB,string ...string)   (interface{},error){
	result := db.Where("username = ?", string[0]).Find(&data)
	if err := result.Error; err != nil {
		return nil,err
	}
	if result.RowsAffected > 0 {
		return nil,errors.New("Username or email is exist")
	}
	return data,nil
}

func (data Users) Update(db *gorm.DB,r *http.Request,string ...string)  (interface{},error) {
	hashid := string[0]
	id,err := helper.DecodeHash(hashid)
	if err != nil {
		return nil,err
	}
	if err := db.Where("id = ?", id).First(&data).Error; err != nil {
		return nil,err
	}
	if err := data.setValue(false);err != nil {
		return nil, err
	}
	if err := db.Select("Username","Password","Updated_at").Save(&data).Error; err != nil {
		return nil,err
	}
	return data,nil
}


func (data *Users) setValue(update bool)  error  {
	if err := helper.ValidateData(data);err != nil {
		return err
	}
	data.GenerateTime(update)
	if err := data.setData();err != nil {
		return err
	}
	return nil
}

func (data *Users) updateHashId(db *gorm.DB)  error {
	enc,err := helper.EncodeHash(int(data.ID))
	if err != nil {
		return err
	}
	log.Print(strconv.Itoa(int(data.ID))+" asd")
	response := db.Model(&data).Where("id",data.ID).Update("hash_id", enc)
	if response.Error != nil{
		return response.Error
	}
	return nil
}

func (data *Users) FindOrCreate(db *gorm.DB,) (interface{},error) {
	tmp  := data
	result := db.Where("username = ?", tmp.Username).Or("email",tmp.Email).Find(&data)
	if err := result.Error; err != nil {
		return nil,err
	}
	if result.RowsAffected > 0 {
		return nil,errors.New("Username or email is exist")
	}
	if err := tmp.setData();err != nil {
		return nil, err
	}
	result = db.Create(tmp)
	if result.Error != nil {
		return nil,result.Error
	}
	if result.RowsAffected < 1 {
		return nil,errors.New("Failed to create data")
	}
	if err := tmp.updateHashId(db);err != nil{
		return nil, err
	}
	tmp.Password = ""
	return tmp,nil
}


func (data *Users) setData() error {
	passs := strings.TrimSpace(data.Password)
	if passs == ""  || len(passs) < 7 {
		return errors.New("Password harus diisi,minimal harus 7 karakter")
	}
	data.Role = "saksi"
	pass := helper.Password{}
	data.Pass = data.Password
	password,err := pass.HashPassword(data.Password)
	if err != nil {
		return err
	}
	data.GenerateTime(false)
	data.Password = password
	return nil
}