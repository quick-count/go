package model

import (
	"errors"
	"gitlab.com/quick-count/go/helper"
	"gorm.io/gorm"
)

type Kelurahan struct {
	ID        		uint `json:"hashid" gorm:"primarykey"`
	HashID 			string `json:"id"  gorm:"type:varchar(255);"`
	Name			string `json:"nama"  validate:"required" gorm:"type:varchar(255);uniqueIndex"`
	Penduduk		uint `json:"penduduk"   gorm:"type:varchar(255);uniqueIndex"`
	KecamatanID 	string `json:"kecamatan_id"   gorm:"type:varchar(255);"`
	helper.TimeModel
}

type Kecamatan struct {
	ID        		uint `json:"hashid" gorm:"primarykey"`
	HashID 			string `json:"id"  gorm:"type:varchar(255);"`
	Name			string `json:"nama"  validate:"required" gorm:"type:varchar(255);uniqueIndex"`
	Penduduk		uint `json:"penduduk"  gorm:"type:varchar(255);uniqueIndex"`
	helper.TimeModel
	//Kelurahan 		[]Kelurahan  `json:"kelurahan"`
}

type KecamatanGetAll struct {
	ID        		uint `json:"hashid" gorm:"primarykey"`
	HashID 			string `json:"id"  gorm:"type:varchar(255);"`
	Name			string `json:"nama"  validate:"required" gorm:"type:varchar(255);uniqueIndex"`
	Penduduk		uint `json:"penduduk"  gorm:"type:varchar(255);uniqueIndex"`
	helper.TimeModel
	//Kelurahan 		[]Kelurahan  `json:"kelurahan"`
}

func (data *Kecamatan) GetAll(db *gorm.DB)   (interface{},error){
	result := []KecamatanGetAll{}
	if err := db.Raw("SELECT * from kecamatans").Scan(&result).Error;err != nil {
		return nil,err
	}
	tmp := []KecamatanGetAll{}
	for _,kec := range result {
		tmpKel := []Kelurahan{}
		//tmpKec := KecamatanGetAll{}
		print(data.ID)
		if err := db.Where("kecamatan_id",data.ID).Find(&tmpKel).Error; err != nil {
			return nil,err
		}
		//kec.Kelurahan = tmpKel
		tmp = append(tmp, kec)
	}
	return tmp,nil
}

func (data *Kelurahan) Create(db *gorm.DB) (interface{},error){
	tmp := data
	// var count int64
	// if err := db.Where("name = ?", data.Name).Count(&count).Error; err != nil {
	//	 return nil,err
	// }
	// if count > 0 {
	//	 return nil,errors.New("Kelurahan sudah ada")
	// }
	if err := tmp.setValue(false);err != nil {
		return nil, err
	}
	result := db.Create(&tmp)
	if result.Error != nil {
		return nil,result.Error
	}
	if result.RowsAffected < 1 {
		return nil,errors.New("Failed to create data")
	}
	if err := tmp.updateHashId(db);err != nil{
		return nil, err
	}
	return tmp,nil
}

func (data *Kelurahan) updateHashId(db *gorm.DB)  error {
	enc,err := helper.EncodeHash(int(data.ID))
	if err != nil {
		return err
	}
	response := db.Model(&data).Where("id",data.ID).Update("hash_id", enc)
	if response.Error != nil{
		return response.Error
	}
	return nil
}

func (data *Kelurahan) setValue(update bool)  error  {
	if err := helper.ValidateData(data);err != nil {
		return err
	}
	data.GenerateTime(update)
	return nil
}

func (data Kelurahan) FindByKecamatan(db *gorm.DB,string ...string) (interface{}, error) {
	result := []Kelurahan{}
	hashid := string[0]
	id,err := helper.DecodeHash(hashid)
	if err != nil{
		return nil,err
	}
	if err = db.Where("kecamatan_id = ?", id).Find(&result).Error; err != nil {
		return nil,err
	}
	return result,nil
}

func (KecamatanGetAll) TableName() string {
	return "kecamatans"
}

func (data *Kecamatan) All(db *gorm.DB)   (interface{},error){
	result := []Kecamatan{}
	if err := db.Find(&result).Error; err != nil {
		return nil,err
	}
	return result,nil
}

func (data *Kecamatan) Create(db *gorm.DB) (interface{},error){
	var count int64
	if err := db.Model(&Kecamatan{}).Where("name = ?", data.Name).Count(&count).Error; err != nil {
		return nil,err
	}
	if count > 0 {
		return nil,errors.New("Kecamatan sudah ada")
	}
	if err := data.setValue(false);err != nil {
		return nil, err
	}
	result := db.Create(&data)
	if result.Error != nil {
		return nil,result.Error
	}
	if result.RowsAffected < 1 {
		return nil,errors.New("Failed to create data")
	}
	if err := data.updateHashId(db);err != nil{
		return nil, err
	}
	return data,nil
}

func (data *Kecamatan) updateHashId(db *gorm.DB)  error {
	enc,err := helper.EncodeHash(int(data.ID))
	if err != nil {
		return err
	}
	response := db.Model(&data).Where("id",data.ID).Update("hash_id", enc)
	if response.Error != nil{
		return response.Error
	}
	return nil
}

func (data *Kecamatan) setValue(update bool)  error  {
	if err := helper.ValidateData(data);err != nil {
		return err
	}
	data.GenerateTime(update)
	return nil
}
