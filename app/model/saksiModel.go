package model

import "gitlab.com/quick-count/go/helper"
type Saksi struct {
	ID        		uint `json:"-" gorm:"primarykey"`
	HashID 			string `json:"id" gorm:"type:varchar(255);"`
	Nik				string `json:"nik"  validate:"required,numeric" gorm:"type:varchar(16);"`
	Name			string `json:"nama"  validate:"required,ascii" gorm:"type:varchar(75);"`
	Gender 			string `json:"jenis_kelamin"  validate:"required,alpha" gorm:"type:varchar(15);"`
	PhoneNumber		string `json:"nomor_telepon"  validate:"required,number" gorm:"type:varchar(17);"`
	Identity		string `json:"link_ktp"  validate:"required,url" gorm:"type:varchar(255);"`
	Alamat			string `json:"alamat"  validate:"required,url" gorm:"type:varchar(255);"`
	Users			Users `json:"user" gorm:"foreignkey:SaksiID;references:ID"`
	helper.TimeModel
}

type SaksiCustom struct {
	ID        		uint `json:"hashid" gorm:"primarykey"`
	HashID 			string `json:"id" gorm:"type:varchar(255);"`
	Nik				string `json:"nik"  validate:"required,numeric" gorm:"type:varchar(16);"`
	Name			string `json:"nama"  validate:"required,ascii" gorm:"type:varchar(75);"`
	Gender 			string `json:"jenis_kelamin"  validate:"required,alpha" gorm:"type:varchar(15);"`
	PhoneNumber		string `json:"nomor_telepon"  validate:"required,number" gorm:"type:varchar(17);"`
	Identity		string `json:"link_ktp"   gorm:"type:varchar(255);"`
	Alamat			string `json:"alamat"   `
}

func (Saksi) TableName() string {
	return "saksi"
}