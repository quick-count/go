package model

import (
	"errors"
	"gitlab.com/quick-count/go/helper"
	"gorm.io/gorm"
	"net/http"
)

func (data SaksiCustom) Create(db *gorm.DB) (interface{},error) {
	tx := db.Begin()
	result := tx.Create(&data)
	if result.Error != nil {
		tx.Rollback()
		return nil,result.Error
	}
	if result.RowsAffected < 1 {
		tx.Rollback()
		return nil,errors.New("Failed to create data")
	}
	if err := data.updateHashId(tx);err != nil{
		tx.Rollback()
		return nil, err
	}
	tx.Commit()
	return data,nil
}


func (data *SaksiCustom) Update(db *gorm.DB,r *http.Request, string ...string)  (interface{},error) {
	hashid := string[0]
	id,err := helper.DecodeHash(hashid)
	if err != nil {
		return nil,err
	}
	user := Users{}
	tmpdata := SaksiCustom{}
	if err := db.Where("id = ?", id).First(&user).Error; err != nil {
		return nil,err
	}
	check := db.Where("id = ?", user.SaksiID).First(&tmpdata)
	if check.Error != nil {
		return nil,err
	}
	if check.RowsAffected < 1 {
		return nil,errors.New("Data tidak ditemukan")
	}

	if err := data.setValue();err != nil {
		return nil, err
	}
	tmpdata.Name = data.Name
	tmpdata.Alamat = data.Alamat
	tmpdata.Nik = data.Nik
	tmpdata.Gender = data.Gender
	tmpdata.Identity = data.Identity
	tmpdata.PhoneNumber = data.PhoneNumber
	if err := db.Save(&tmpdata).Error; err != nil {
		return nil,err
	}
	return data,nil
}

func (data *SaksiCustom) setValue()  error  {
	if err := helper.ValidateData(data);err != nil {
		return err
	}
	return nil
}

func (data *SaksiCustom) updateHashId(db *gorm.DB)  error {
	enc,err := helper.EncodeHash(int(data.ID))
	if err != nil {
		return err
	}
	response := db.Model(&data).Update("hash_id", enc)
	if response.Error != nil{
		return response.Error
	}
	return nil
}
