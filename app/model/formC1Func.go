package model

import (
	"errors"
	"gitlab.com/quick-count/go/helper"
	"gorm.io/gorm"
	"net/http"
	"strconv"
)


func (data *FormC1Payload) Convert(model *FormC1Model) (err error){
	if err := helper.ValidateData(data);err != nil {
		return err
	}
	model.UserID,err = helper.DecodeHash(data.UserID)
	if err != nil {
		return err
	}
	model.TpsID,err = helper.DecodeHash(data.TpsID)
	if err != nil {
		return err
	}
	model.HasilSuaraSatu = data.HasilSuaraSatu
	model.HasilSuaraKotakKosong = data.HasilSuaraKotakKosong
	model.TotalSuaraMasuk = data.TotalSuaraMasuk
	model.SuaraTidakSah = data.SuaraTidakSah
	return nil
}

func (data FormC1Model) Create(db *gorm.DB) (string,interface{},error) {
	if err := data.setValue(false);err != nil {
		return "",nil, err
	}
	status := "Created"
	tx := db.Begin()
	tmpWillCreate := data
	user := Users{}
	check := db.Where("id",data.UserID).First(&user)
	if check.RowsAffected < 1 {
		return "",nil,errors.New("User tidak dapat ditemukan")
	}
	tmpWillCreate.UserID = user.ID
	tmpWillCreate.TpsID = user.TpsID
	check = db.Where("user_id",data.UserID).Find(&data)

	isCreate := false
	if check.Error != nil {
		if check.Error.Error() == "record not found" {
			isCreate = true
		}else{
			return "", nil, check.Error
		}
	}
	if check.RowsAffected > 0 && isCreate == false{
		data.HasilSuaraKotakKosong = tmpWillCreate.HasilSuaraKotakKosong
		data.HasilSuaraSatu = tmpWillCreate.HasilSuaraSatu
		data.SuaraTidakSah = tmpWillCreate.SuaraTidakSah
		data.TotalSuaraMasuk = tmpWillCreate.TotalSuaraMasuk
		check := db.Save(&data)
		if check.Error != nil {
			return "",nil,check.Error
		}
		status = "Updated"
	}else{
		check = tx.Create(&tmpWillCreate)
		if check.Error != nil {
			tx.Rollback()
			return "",nil,check.Error
		}
		if check.RowsAffected < 1 {
			tx.Rollback()
			return "",nil,errors.New("Failed to create data a")
		}
		if err := tmpWillCreate.updateHashId(tx);err != nil{
			tx.Rollback()
			return "",nil, err
		}
	}
	tx.Commit()
	return status,data,nil
}


func (data FormC1Model) All(db *gorm.DB,string ...string) (interface{}, error) {
	result := []FormC1Model{}
	limit,err := strconv.Atoi(string[1])
	if err != nil {
		return nil, err
	}
	trans := db.Limit(limit).Joins("Users").Find(&result)
	hashID := string[0]
	if hashID != "" {
		id,err := helper.DecodeHash(hashID)
		if err != nil {
			return nil,err
		}
		trans = trans.Where("id > ?",id).Find(&result)
	}
	exec := trans.Find(&result)
	if exec.Error != nil {
		return result,exec.Error
	}
	return result,nil
}

func (data FormC1Model) Find(db *gorm.DB,string ...string)   (interface{},error){
	hashid := string[0]
	id,err := helper.DecodeHash(hashid)
	if err = db.Where("user_id = ?", id).First(&data).Error; err != nil {
		return nil,err
	}
	return data,nil
}

func (data FormC1Model) Update(db *gorm.DB,r *http.Request, string ...string)  (interface{},error) {
	hashid := string[0]
	id,err := helper.DecodeHash(hashid)
	if err != nil {
		return nil,err
	}
	if err := db.Where("id = ?", id).First(&data).Error; err != nil {
		return nil,err
	}
	if err := data.setValue(false);err != nil {
		return nil, err
	}
	if err := db.Save(&data).Error; err != nil {
		return nil,err
	}
	return data,nil
}

func (data FormC1Model) Delete(db *gorm.DB,string ...string) (interface{},error){
	hashid := string[0]
	id,err := helper.DecodeHash(hashid)
	if err != nil {
		return nil,err
	}
	tx := db.Begin()
	if err := tx.Where("id = ?", id).First(&data).Error; err != nil {
		tx.Rollback()
		return nil,err
	}
	response := tx.Where("saksi_id = ?",id).Delete(Users{})
	if response.Error != nil {
		tx.Rollback()
		return nil,response.Error
	}
	response = tx.Where("id = ?",id).Delete(FormC1Model{})
	if response.Error != nil {
		tx.Rollback()
		return nil,response.Error
	}
	tx.Commit()
	return data,nil
}

func (data *FormC1Model) setValue(update bool)  error  {
	if err := helper.ValidateData(data);err != nil {
		return err
	}
	data.GenerateTime(update)
	return nil
}

func (data *FormC1Model) updateHashId(db *gorm.DB)  error {
	enc,err := helper.EncodeHash(int(data.ID))
	if err != nil {
		return err
	}
	response := db.Model(&data).Update("hash_id", enc)
	if response.Error != nil{
		return response.Error
	}
	return nil
}

