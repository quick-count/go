package app

import (
	"github.com/gorilla/mux"
	authHelper "gitlab.com/quick-count/go/auth/services"
	"gitlab.com/quick-count/go/config"
	"gitlab.com/quick-count/go/helper"
	"gitlab.com/quick-count/go/app/handler"
	"gorm.io/driver/mysql"
	"gorm.io/gorm"
	"log"
	"net/http"
	"time"
)

// App has router and db instances
type SaksiApp struct {
	Router *mux.Router
	DB     *gorm.DB
}

// Initialize initializes the app with predefined configuration
func (a *SaksiApp) Initialize(config *config.Config,route *mux.Router) {
	db, err := gorm.Open(mysql.Open(config.GetDSN()), &gorm.Config{
		PrepareStmt: true,
		SkipDefaultTransaction: true,
	})
	if err != nil {
		log.Fatal("Could not connect database")
	}
	sqlDB, err := db.DB()
	// SetMaxIdleConns sets the maximum number of connections in the idle connection pool.
	sqlDB.SetMaxOpenConns(25)
	sqlDB.SetMaxIdleConns(25)
	sqlDB.SetConnMaxLifetime(5*time.Minute)
	a.DB = db
	a.Router = route
	a.setRouters()
	log.Println("app server is running")
}

// setRouters sets the all required routers
func (a *SaksiApp) setRouters() {
	//// Routing for handling the projectsUserFind
	a.Get("/kecamatan", a.guard(handler.KecamatanAll))
	a.Get("/kelurahan/{hashid}", a.guard(handler.KelurahanAll))


	a.Post("/admin/kecamatan", a.guardAdmin(handler.KecamatanCreate))
	a.Post("/admin/kelurahan", a.guardAdmin(handler.KelurahanCreate))
	a.Post("/admin/saksi", a.guardAdmin(handler.SaksiCreate))
	a.Post("/admin/kelurahan", a.guardAdmin(handler.UserCreate))
	a.Post("/admin/tps", a.guardAdmin(handler.TPSCreate))
	a.Post("/admin/user", a.guardAdmin(handler.UserCreate))


	a.Get("/user/{hashid}", a.guardSaksi(handler.UserFind))
	a.Post("/formc1", a.guardSaksi(handler.FormC1Create))
	a.Get("/formc1/{hashid}", a.guardSaksi(handler.FormC1Find))
	a.Post("/formc1/{hashid}/upload", a.guardSaksi(handler.UploadImage))
	a.Put("/saksi/{hashid}", a.guardSaksi(handler.SaksiUpdateWithoutUser))
	a.Post("/saksi/{hashid}/upload", a.guardSaksi(handler.UploadKTP))

	a.Get("/get/user/{hashid}", a.guardAdmin(handler.UserFind))
	a.Post("/users/getall", a.guardAdmin(handler.GetAllUser))
	a.Put("/saksi/{hashid}", a.guardAdmin(handler.SaksiUpdate))

	a.Post("/admin/formc1", a.guardAdmin(handler.FormC1Create))
	a.Get("/admin/formc1/{hashid}", a.guardAdmin(handler.FormC1Find))
	a.Post("/admin/formc1/{hashid}/upload", a.guardAdmin(handler.UploadImage))

	a.Get("/rekap", a.guard(handler.GetRekap))
	a.Get("/rekap/kelurahan/{hashid}", a.guard(handler.GetRekapKelurahan))
	a.Get("/rekap/kecamatan/{hashid}", a.guard(handler.GetRekapKecamatan))
	a.Get("/rekap/formc1/kelurahan/{hashid}", a.guardAdmin(handler.GetRekapFormC1ByKelurahan))

	a.Get("/rekap/tamu", a.guest(handler.GetRekap))

}


// Get wraps the router for GET method
func (a *SaksiApp) Get(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("GET")
}

// Put wraps the router for PUT method
func (a *SaksiApp) Put(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("PUT")
}

// Post wraps the router for POST method
func (a *SaksiApp) Post(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("POST")
}

// Delete wraps the router for DELETE method
func (a *SaksiApp) Delete(path string, f func(w http.ResponseWriter, r *http.Request)) {
	a.Router.HandleFunc(path, f).Methods("DELETE")
}

// Run the app on it's router
func (a *SaksiApp) Run(host string) {
	log.Fatal(http.ListenAndServe(host, a.Router))
}

type RequestHandlerFunction func(db *gorm.DB, w http.ResponseWriter, r *http.Request)

func (a *SaksiApp) guest(handler RequestHandlerFunction) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		handler(a.DB, w, r)
	}
}

func (a *SaksiApp) guard(handler RequestHandlerFunction) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := authHelper.Authorization(a.DB,r,"*");err != nil {
			helper.RespondJSONError(w,http.StatusUnauthorized,err)
			return
		}
		handler(a.DB, w, r)
	}
}

func (a *SaksiApp) guardAdmin(handler RequestHandlerFunction) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		if err := authHelper.Authorization(a.DB,r,"admin");err != nil {
			helper.RespondJSONError(w,http.StatusUnauthorized,err)
			return
		}
		handler(a.DB, w, r)
	}
}

func (a *SaksiApp) guardClient(handler RequestHandlerFunction) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		helper.CorsHelper(w,r)
		if r.Method == http.MethodOptions {
			return
		}
		if err := authHelper.Authorization(a.DB,r,"client");err != nil {
			helper.RespondJSONError(w,http.StatusUnauthorized,err)
			return
		}
		handler(a.DB, w, r)
	}
}



func (a *SaksiApp) guardSaksi(handler RequestHandlerFunction) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		helper.CorsHelper(w,r)
		if r.Method == http.MethodOptions {
			return
		}
		if err := authHelper.Authorization(a.DB,r,"saksi");err != nil {
			helper.RespondJSONError(w,http.StatusUnauthorized,err)
			return
		}
		handler(a.DB, w, r)
	}
}




