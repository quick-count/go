package handler

import (
	"errors"
	"gitlab.com/quick-count/go/app/model"
	"gitlab.com/quick-count/go/helper"
	"gorm.io/gorm"
	"net/http"
)
func KecamatanAll(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.Kecamatan{}
	data,err := serv.All(db)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, "Success",http.StatusOK, data)
}

func KecamatanKelAll(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.Kecamatan{}
	data,err := serv.GetAll(db)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, "Success",http.StatusOK, data)
}

func KelurahanAll(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.Kelurahan{}
	hashID := helper.DecodeHashID(r)
	if hashID == ""{
		helper.RespondJSONError(w, http.StatusBadRequest, errors.New("Invalid kecamatan id"))
		return
	}
	data,err := serv.FindByKecamatan(db,hashID)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, "Success",http.StatusOK, data)
}

func KecamatanCreate(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.Kecamatan{}
	if err := helper.DecodeJson(r,&serv);err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	data,err := serv.Create(db)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, "Success",http.StatusOK, data)
}

func KelurahanCreate(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.Kelurahan{}
	if err := helper.DecodeJson(r,&serv);err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	data,err := serv.Create(db)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, "Success",http.StatusOK, data)
}

func TPSCreate(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.TPS{}
	if err := helper.DecodeJson(r,&serv);err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	data,err := serv.Create(db)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, "Success",http.StatusOK, data)
}

func SaksiCreate(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.SaksiCustom{}
	if err := helper.DecodeJson(r,&serv);err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	data,err := serv.Create(db)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, "Success",http.StatusOK, data)
}


func UserCreate(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.Users{}
	if err := helper.DecodeJson(r,&serv);err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	data,err := serv.FindOrCreate(db)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, "Success",http.StatusOK, data)
}

