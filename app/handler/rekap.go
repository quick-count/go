package handler

import (
	"errors"
	"github.com/gorilla/mux"
	"gitlab.com/quick-count/go/app/model"
	"gitlab.com/quick-count/go/helper"
	"gorm.io/gorm"
	"net/http"
)

func GetRekap(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.RekapSuara{}
	data,err := serv.GetRekap(db)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, "Success",http.StatusOK, data)
}

func GetRekapKecamatan(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.RekapSuara{}
	hashID := helper.DecodeHashID(r)
	if hashID == ""{
		helper.RespondJSONError(w, http.StatusBadRequest, errors.New("Invalid kecamatan id"))
		return
	}
	data,err := serv.GetRekapByKecamatans(db,hashID)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, "Success",http.StatusOK, data)
}


func GetRekapKelurahan(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.RekapSuara{}
	hashID := helper.DecodeHashID(r)
	if hashID == ""{
		helper.RespondJSONError(w, http.StatusBadRequest, errors.New("Invalid kecamatan id"))
		return
	}
	data,err := serv.GetRekapByKelurahans(db,hashID)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, "Success",http.StatusOK, data)
}


func GetRekapFormC1ByKelurahan(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	hashid,exist := vars["hashid"]
	if !exist{
		helper.RespondJSONError(w, http.StatusBadRequest, errors.New("Invalid id"))
		return
	}
	serv := model.FormC1Model{}
	//request := model.Saksi{}
	data,err := serv.AllByKelurahan(db,hashid)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, "Succes",http.StatusOK, data)
}
