package handler

import (
	"errors"
	"gitlab.com/quick-count/go/app/model"
	"gitlab.com/quick-count/go/helper"
	"gorm.io/gorm"
	"net/http"
)

func UploadImage(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	data := model.FormC1Model{}
	hashID := helper.DecodeHashID(r)
	if hashID == ""{
		helper.RespondJSONError(w, http.StatusBadRequest,errors.New("Invalid id"))
		return
	}
	response, err := data.UploadImg(db,r,hashID)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest,err)
		return
	}
	helper.RespondJSON(w,"Succes", http.StatusOK, response)
	return
}


func UploadKTP(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	data := model.SaksiCustom{}
	hashID := helper.DecodeHashID(r)
	if hashID == ""{
		helper.RespondJSONError(w, http.StatusBadRequest,errors.New("Invalid id"))
		return
	}
	response, err := data.UploadImg(db,r,hashID)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest,err)
		return
	}
	helper.RespondJSON(w,"Succes", http.StatusOK, response)
	return
}