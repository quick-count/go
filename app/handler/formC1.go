package handler

import (
	"errors"
	"github.com/gorilla/mux"
	"gitlab.com/quick-count/go/app/model"
	"gitlab.com/quick-count/go/helper"
	"gorm.io/gorm"
	"net/http"
)


func FormC1Find(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	hashid,exist := vars["hashid"]
	if !exist{
		helper.RespondJSONError(w, http.StatusBadRequest, errors.New("Invalid id"))
		return
	}
	serv := model.FormC1Model{}
	//request := model.Saksi{}
	data,err := serv.Find(db,hashid)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, "Succes",http.StatusOK, data)
}




func FormC1Create(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	payload := model.FormC1Payload{}
	if err := helper.DecodeJson(r,&payload);err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	serv := model.FormC1Model{}
	err := payload.Convert(&serv)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	//token := authModel.TokenJWT{}
	//tokenData,_,err := services.DecodeToken(&token,r)
	//if err != nil {
	//	helper.RespondJSONError(w, http.StatusBadRequest, err)
	//	return
	//}
	//payload.UserID = tokenData["user"].(string)
	status,data,err := serv.Create(db)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, status,http.StatusOK, data)
}



