package handler

import (
	"errors"
	"github.com/gorilla/mux"
	"gitlab.com/quick-count/go/helper"
	"gitlab.com/quick-count/go/app/model"
	"gorm.io/gorm"
	"net/http"
)


func GetAllUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.RequestGetAllUser{}
	if err := helper.DecodeJson(r,&serv);err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	data,err := model.GetAllUser(db,serv)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w, "Success",http.StatusOK, data)
}



func UserFind(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.Users{}
	vars := mux.Vars(r)
	hashid,exist := vars["hashid"]
	if !exist{
		helper.RespondJSONError(w, http.StatusBadRequest, errors.New("Invalid id"))
		return
	}
	//request := model.Saksi{}
	data,err := serv.FindOri(db,hashid)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}
	helper.RespondJSON(w,"Succes", http.StatusOK, data)
}

//func UserUpdate(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
//	serv := model.Users{}
//	if err := helper.DecodeJson(r,&serv);err != nil {
//		helper.RespondJSONError(w, http.StatusBadRequest, err)
//		return
//	}
//	hashID := helper.DecodeHashID(r)
//	if hashID != ""{
//		helper.RespondJSONError(w, http.StatusBadRequest, errors.New("Invalid id"))
//		return
//	}
//	data,err := serv.Update(db,r,hashID)
//	if err != nil {
//		helper.RespondJSONError(w, http.StatusBadRequest, err)
//		return
//	}
//
//	helper.RespondJSON(w, http.StatusOK, data)
//}
