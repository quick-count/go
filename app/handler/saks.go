package handler

import (
	"errors"
	"github.com/gorilla/mux"
	"gitlab.com/quick-count/go/app/model"
	"gitlab.com/quick-count/go/helper"
	"gorm.io/gorm"
	"net/http"
)

func SaksiUpdate(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.Saksi{}
	vars := mux.Vars(r)
	hashid,exist := vars["hashid"]
	if !exist{
		helper.RespondJSONError(w, http.StatusBadRequest, errors.New("Invalid id"))
		return
	}
	data,err := serv.Update(db,r,hashid)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}

	helper.RespondJSON(w, "Updated",http.StatusOK, data)
}

func SaksiUpdateWithoutUser(db *gorm.DB, w http.ResponseWriter, r *http.Request) {
	serv := model.SaksiCustom{}
	if err := helper.DecodeJson(r,&serv);err != nil{
		helper.RespondJSONError(w, http.StatusBadRequest, errors.New("Invalid payload"))
		return
	}
	vars := mux.Vars(r)
	hashid,exist := vars["hashid"]
	if !exist{
		helper.RespondJSONError(w, http.StatusBadRequest, errors.New("Invalid id"))
		return
	}
	data,err := serv.Update(db,r,hashid)
	if err != nil {
		helper.RespondJSONError(w, http.StatusBadRequest, err)
		return
	}

	helper.RespondJSON(w, "Updated",http.StatusOK, data)
}



