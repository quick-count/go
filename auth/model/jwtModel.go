package model

type TokenJWT struct {
	User string
	Username string
	Name string
	Role string
	Scope string
	Expired string
	Token string
}