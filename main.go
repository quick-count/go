package main

import (
	"github.com/gorilla/mux"
	"gitlab.com/quick-count/go/app"
	"gitlab.com/quick-count/go/auth"
	"gitlab.com/quick-count/go/config"
	"net/http"
)


type CORSRouterDecorator struct {
	R *mux.Router
}

// ServeHTTP wraps the HTTP server enabling CORS headers.
// For more info about CORS, visit https://www.w3.org/TR/cors/

func main() {
	r := mux.NewRouter()
	config := config.GetConfig()
	auth := &auth.Auth{}
	auth.Initialize(config,r)
	saksi := &app.SaksiApp{}
	saksi.Initialize(config,r)
	http.ListenAndServe(":8080", r)
}

