module gitlab.com/quick-count/go

go 1.15

require (
	github.com/cristalhq/jwt/v3 v3.0.4 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/go-playground/validator/v10 v10.4.1
	github.com/google/uuid v1.1.2
	github.com/gorilla/handlers v1.5.1 // indirect
	github.com/gorilla/mux v1.8.0
	golang.org/x/crypto v0.0.0-20201112155050-0c6587e931a9
	golang.org/x/sys v0.0.0-20201113233024-12cec1faf1ba // indirect
	gorm.io/driver/mysql v1.0.3
	gorm.io/gorm v1.20.6
)
